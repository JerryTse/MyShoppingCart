﻿var cart = (function () {
    // shopping cart object
    var Cart = { cartName: "My Shopping Cart", cartItems: new Array() };

    return {
        constructor: function (cartName, cartItems) {
            Cart.cartName = cartName;
            Cart.cartItems = cartItems;
        },
        // function to return cart items list
        getCartItems: function () {
            return Cart.cartItems;
        },
        // add new item to shopping cart method
        addItemToCart: function (product) {
            var index = 0;

            // check if it is contains in the shopping cart
            function containsItem(name, list) {
                var i;
                for (i = 0; i < list.length; i++) {
                    if (list[i].name === name) {
                        index = i;
                        return true;
                    }
                }
                return false;
            }

            if (containsItem(product.name, Cart.cartItems)) {
                // if so, quantity plus 1
                Cart.cartItems[index].quantity += 1;
            } else {
                // else add new item
                Cart.cartItems.push(product);
            }

            // Refresh the cart list in UI and calculate total amount
            this.displayCartList();
            this.calculateTotal();
        },
        // Method to refresh the shopping cart list in UI
        displayCartList: function () {
            var table = document.getElementById('list');
            table.innerHTML = '<tr><td>Name</td><td>Price</td><td>Quantity</td></tr>';
            for (var i = 0; i < Cart.cartItems.length; i++) {
                var node = undefined;
                var note = Cart.cartItems[i];
                var node = document.createElement('tr');
                var html = '<td>' + note.name + '</td><td>' + note.price + '</td><td><span class="btn" href="#" onClick="increaseQuantity(' + i + ')">+</span>' + note.quantity + '<span class="btn" href="#" onClick="decreaseQuantity(' + i + ')">-</span></td><td><span class="btn btn-primary" href="#" onClick="deleteIndex(' + i + ')">delete</span></td>';
                node.innerHTML = html;
                table.appendChild(node);
            }
        },
        // Method to calculate the total amount and display in UI
        calculateTotal: function () {
            var total = document.getElementById("total");
            var count = 0;

            function calculate(cartItems) {
                Cart.cartItems = cartItems;

                for (var i = 0; i < Cart.cartItems.length; i++) {
                    count = count + Cart.cartItems[i].price * Cart.cartItems[i].quantity;
                }

                return count;
            };

            var displayTotal = function (totalAmount) {
                total.innerHTML = "$" + totalAmount;
            }

            displayTotal(calculate(Cart.cartItems));
        },
        // Method to remove the cart item
        removeItem: function (i) {
            Cart.cartItems.splice(i, 1);
            this.displayCartList();
            this.calculateTotal();
        },
        // Method to increase the item quantity
        increaseQuantity: function (i) {
            Cart.cartItems[i].quantity += 1;
            this.displayCartList();
            this.calculateTotal();
        },
        // Method to decrease the item quantity
        decreaseQuantity: function (i) {
            if (Cart.cartItems[i].quantity > 1) {
                Cart.cartItems[i].quantity -= 1;
                this.displayCartList();
                this.calculateTotal();
            }
        }
    }
})();
    