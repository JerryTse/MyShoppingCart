﻿var products = new Array();
var records;

// initialize the web page
function init() {
    // create promise object to get product list in json type
    let getJson = new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://api.themoviedb.org/3/movie/top_rated?api_key=a0a7e40dc8162ed7e37aa2fc97db5654', true);
        xhr.responseType = 'json';

        xhr.onload = function () {
            var status = xhr.status;

            if (status == 200) {
                resolve(xhr.response);
            } else {
                reject(status);
            }
        };

        xhr.send();
    });

    // after received json data, generate the product list in UI
    getJson.then((data) => { // If the return is resolved, the returned promise will "follow" that thenable logic 
        console.log(data);

        var productTable = document.getElementById("productList");

        for (var i = 0; i < data.results.length; i++) {
            var newProduct = new product(data.results[i].id, data.results[i].title, Math.round(data.results[i].popularity * 4));
            newProduct.img = "http://image.tmdb.org/t/p/w500" + data.results[i].poster_path;
            products.push(newProduct);
            var node = undefined;
            var node = document.createElement('div');
            node.className = 'col-md-3 mb-3 card h-100';
            var html = '<img src="' + newProduct.img + '"/><p style="height: 30px">' + newProduct.name + '</p><p>$' + newProduct.price + '</p><button class="btn btn-primary" href="#" onClick="addItem(' + i + ')" />Add To Cart';
            node.innerHTML = html;
            productTable.appendChild(node);
        }

    }).catch((reason) => { // If the return is rejected, the returned promise will "follow" that catch logic 
        console.error(reason);
        });

    // retrieve cart items record from last time
    const getRecords = new Promise((resolve, reject) => {
        var reD = openDB(get, cart.getCartItems());
        if (records != undefined) {
            resolve();
        } else {
            reject();
        }
    });
    getRecords.then(() => { }).catch(() => {});
}

window.onload = init;

// cart item click event function
function deleteIndex(i) {
    cart.removeItem(i);
}

// add item to cart click event function
function addItem(i) {
    var item = new product(products[i].id, products[i].name, products[i].price);
    item.quantity = 1;
    cart.addItemToCart(item);
}

// increase cart item quantity click event function
function increaseQuantity(i) {
    cart.increaseQuantity(i);
}

// decrease cart item quantity click event function
function decreaseQuantity(i) {
    cart.decreaseQuantity(i);
}

// checkout click event function
function checkout() {
    // check if the cart is empty
    if (cart.getCartItems() != 0) {
        // if succeed, save the cart items to DB
        openDB(save, cart.getCartItems());
        alert("Checkout succeed, click ok to redirect to payment page.");
        window.location.reload();
    } else {
        // else prompt error message
        alert("Your cart is empty.")
    }
}

// take the retrieved records data to show history
function viewHistory() {
    var history = "";
    var historyTotal = 0;

    for (var i = 0; i < records.length; i++) {
        history += "Name:" + records[i].name + " Price:" + records[i].price + " Quantity:" + records[i].quantity + "\n";
        historyTotal += records[i].price * records[i].quantity;
    }

    history += "Total:" + historyTotal;

    alert(history);
}
