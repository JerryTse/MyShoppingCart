﻿// constants string values
const save = "save";
const get = "get";

// function to open the indexeddb
var openDB = function (action, items) {
    // This works on all devices/browsers, and uses IndexedDBShim as a final fallback 
    var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;

    // Open (or create) the database
    var open = indexedDB.open("MyDatabase", 1);

    // Create the schema
    open.onupgradeneeded = function () {
        var db = open.result;
        var store = db.createObjectStore("products", { keyPath: "id" });
        for (var i in items) {
            store.add(items[i]);
        }
    };

    open.onsuccess = function () {
        // Start a new transaction
        var db = open.result;
        var tx = db.transaction("products", "readwrite");
        var store = tx.objectStore("products");

        // Query the data
        var getAll = store.getAll();

        getAll.onsuccess = function () {
            if (action === save) {
                checkCreateOrUpdate(getAll.result);
            }
            if (action === get) {
                records = getAll.result;
            }
        };

        // Close the db when the transaction is done
        tx.oncomplete = function (data) {
            db.close();
        };
    }

    // compare the db records and decide create, update or delete action
    var checkCreateOrUpdate = function (allData) {
        var db = open.result;
        var tx = db.transaction("products", "readwrite");

        var containsItem = function (name) {
            for (var i = 0; i < allData.length; i++) {
                if (allData[i].name === name) {
                    return true;
                }
            }
            return false;
        }

        var checkRemove = function () {
            var checkExist = function (name) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i].name === name) {
                        return true;
                    }
                }
                return false;
            }

            for (var i = 0; i < allData.length; i++) {
                if (!checkExist(allData[i].name)) {
                    // Delete record
                    var request = tx.objectStore("products").delete(allData[i].id);
                }
            }
            return false;
        }

        for (var i = 0; i < items.length; i++) {
            if (containsItem(items[i].name)) {
                // Update record
                var request = tx.objectStore("products").put(items[i]);
            } else {
                // Create new record
                var request = tx.objectStore("products").add(items[i]);
            }

        }

        checkRemove();

        tx.oncomplete = function () {
            db.close();
        };
    }
}